#pragma once
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include "json.hpp"


using json = nlohmann::json;
class Config
{
private:
	std::vector<std::string> legalExtenctions;
	std::string path;
	bool save();
	
	int CooldownOnChangingPicInMs = 500;

public:
	Config(std::string  path_ = ".");
	void setDefault();
	void addExtenction(std::string ToAdd);    
	void eraseExtenction(std::string ToErase);
	const std::vector<std::string> getExtenctions();
	int getPicCahngeCooldown();
	void setPicCahngeCooldown(int in);

	#ifdef _DEBUG
	void printAlllExtenctions() {
		for (auto ex : legalExtenctions)
			std::cout << ex;
	}
	#endif
};

