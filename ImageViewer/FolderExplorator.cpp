#include "FolderExplorator.h"



namespace fs = std::filesystem;
template <typename TP>
std::time_t to_time_t(TP tp)
{
	using namespace std::chrono;
	auto sctp = time_point_cast<system_clock::duration>(tp - TP::clock::now()
		+ system_clock::now());
	return system_clock::to_time_t(sctp);
}
const std::string FolderExplorator::FileStruct::getStringDate() const {
	std::time_t tt = to_time_t(date);
	std::tm* gmt = std::gmtime(&tt);
	std::stringstream buffer;
	buffer << std::put_time(gmt, "%A, %d %B %Y %H:%M");
	return  buffer.str();
}
bool  FolderExplorator::FileStruct::operator==(const FileStruct& other) {
	return (path == other.path);
}

std::ostream& operator<<(std::ostream& out, const FolderExplorator::FileStruct& f) {
	return out << " Path:" << f.path << " size:" << f.size << " ext:" << f.extension << " name:" << f.name << " date:" << f.getStringDate() << std::endl;
}

FolderExplorator::FolderExplorator(std::string path, std::vector<std::string> extenctions) : files(), pointer(0) {
	
	LoadFilesFromFolder(path, extenctions);
	if(files.size()!=0)
		sort(sortType::ByDate);
}

void FolderExplorator::LoadFilesFromFolder(std::string path, std::vector<std::string> extenctions) {
	for (const auto& entry : std::filesystem::directory_iterator(path)) {
		for (const auto& ext : extenctions) {
			if (ext.compare(entry.path().extension().string()) == 0) {
				files.push_back(
					FileStruct(
						entry.file_size(),
						entry.path().string(),
						entry.last_write_time(),
						entry.path().extension().string(),
						entry.path().filename().string()
					)
				);
				break;
			}
		}
	}
}

bool FolderExplorator::sort(sortType wantedSort) {
	FileStruct currentlyPointed = files[pointer];
	if (currentSort == wantedSort) return false;
	switch (wantedSort) {
	case ByName:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.name > b.name; });
		break;
	case ByDate:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.date > b.date; });
		break;
	case BySize:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.size > b.size; });
		break;
	case ByType:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.extension > b.extension; });
		break;
	case ByNameReversed:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.name < b.name; });
		break;
	case ByDateReversed:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.date < b.date; });
		break;
	case BySizeReversed:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.size < b.size; });
		break;
	case ByTypeReversed:
		std::sort(files.begin(), files.end(), [](const FileStruct& a, const FileStruct& b) {return a.extension < b.extension; });
		break;

	}
	pointer = find(files.begin(), files.end(), currentlyPointed) - files.begin(); 
	currentSort = wantedSort;
	return true;
}

std::string FolderExplorator::ChangeFile(int i) { //to update
	pointer += i;
	if (pointer > 0) pointer %= files.size(); 
	while (pointer < 0) pointer += files.size();
	assert(pointer<0 || pointer>files.size());
	return files[pointer].name;
}
std::string FolderExplorator::getCurrentFilePath() {
	return files[pointer].path;
}
std::string FolderExplorator::getCurrentFileName() {
	return files[pointer].name;
}

std::string FolderExplorator::getCurrentFileLastChanges() {
	return files[pointer].getStringDate();
}

void FolderExplorator::changeSort(int i) {
	sort(sortType(i));
}
FolderExplorator::sortType FolderExplorator::GetCurrentSort() {
	return currentSort;
}
int FolderExplorator::getPicCount() {
	return files.size();
}
int FolderExplorator::getCurrentNumberInVector() {
	return pointer;
}
std::string FolderExplorator::currentSortToString() {
	switch (currentSort) {
	case ByName:
		return "ByName";
		break;
	case ByDate:
		return "ByDate";
		break;
	case BySize:
		return "BySize";
		break;
	case ByType:
		return "ByType";
		break;
	case ByNameReversed:
		return "ByNameReversed";
		break;
	case ByDateReversed:
		return "ByDateReversed";
		break;
	case BySizeReversed:
		return "BySizeReversed";
		break;
	case ByTypeReversed:
		return "ByTypeReversed";
		break;
	}
	return "error";
}