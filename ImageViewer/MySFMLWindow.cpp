#include "MySFMLWindow.h"

std::string MySFMLWindow::generateWindowName() {
	std::string out = "ImageViewer.exe  \tSortType: "+ fileExplorator.currentSortToString()+ "  \tNumber in vector: "+ std::to_string(fileExplorator.getCurrentNumberInVector())+"  \tLastChangeDate: "+ fileExplorator.getCurrentFileLastChanges()+ "  \tFileName: " + fileExplorator.getCurrentFileName();
	return out;
}
void MySFMLWindow::ImageHandeling() {
	AsyncImageLoader::checkIfOk();
	if (!AsyncImageLoader::isServed) {
		if (AsyncImageLoader::img.getSize() != sf::Vector2u(0, 0)) { // isn't empty picture
			int picX = AsyncImageLoader::img.getSize().x,
				picY = AsyncImageLoader::img.getSize().y,
				windowX = window.getSize().x,
				windowY = window.getSize().y;
			float scale = std::min((float)windowX / picX, (float)windowY / picY);
			sf::Vector2f linearScaleToFitFrame(scale, scale);
			
			sprite.setTextureRect(sf::IntRect(sf::Vector2i(0, 0), sf::Vector2i(picX, picY)));
			sprite.setScale(linearScaleToFitFrame);
			float xOffset = (windowX - picX * scale) / 2;
			float yOffset = (windowY - picY * scale) / 2;
			sprite.setPosition(xOffset, yOffset);
		}
		window.clear();
		window.draw(sprite);
		window.display();
		AsyncImageLoader::isServed = true;
	}
}


void MySFMLWindow::KeyboardHandeling() {
	//works always with single click

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
		if (!left) {
			fileExplorator.ChangeFile(1);
			window.setTitle(generateWindowName());
			AsyncImageLoader::loadAsync(fileExplorator.getCurrentFilePath());
			lastChange = std::clock();
		}
		left = true;
	}
	else left = false;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
		if (!right) {
			fileExplorator.ChangeFile(-1);
			window.setTitle(generateWindowName());
			AsyncImageLoader::loadAsync(fileExplorator.getCurrentFilePath());
			lastChange = std::clock();
		}
		right = true;
	}
	else right = false;

	//works while pressing
	if ((std::clock() - lastChange) > conf.getPicCahngeCooldown())
	{
		bool noChanges = false;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			fileExplorator.ChangeFile(1);
			window.setTitle(generateWindowName());
			AsyncImageLoader::loadAsync(fileExplorator.getCurrentFilePath());
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			fileExplorator.ChangeFile(-1);
			window.setTitle(generateWindowName());
			AsyncImageLoader::loadAsync(fileExplorator.getCurrentFilePath());
		}
		else {
			noChanges = true;
		}

		if (!noChanges) {
			lastChange = std::clock();
		}
	}
	bool changes = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad1))
		changes = (fileExplorator.sort(FolderExplorator::ByName));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad2))
		changes = (fileExplorator.sort(FolderExplorator::ByDate));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad3))
		changes = (fileExplorator.sort(FolderExplorator::BySize));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num4) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad4))
		changes = (fileExplorator.sort(FolderExplorator::ByType));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num5) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad5))
		changes = (fileExplorator.sort(FolderExplorator::ByNameReversed));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num6) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad6))
		changes = (fileExplorator.sort(FolderExplorator::ByDateReversed));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num7) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad7))
		changes = (fileExplorator.sort(FolderExplorator::BySizeReversed));
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num8) || sf::Keyboard::isKeyPressed(sf::Keyboard::Numpad8))
		changes = (fileExplorator.sort(FolderExplorator::ByTypeReversed));

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
		exit = true;
	}
	if (changes)
		window.setTitle(generateWindowName());
#ifdef _DEBUG
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) { //debbug
		fileExplorator.showFiles();
	}
#endif
	
}


void MySFMLWindow::EventLoop() {
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
			exit = true;
		}
		if (event.type == sf::Event::Resized)
		{
			// TO DO
		}
	}

}


std::string MySFMLWindow::findPicPath() { 
	wchar_t* path = NULL;
	SHGetKnownFolderPath(FOLDERID_Pictures, 0, NULL, &path);
	std::wstring ws(path);
	return std::string(ws.begin(), ws.end());
}


MySFMLWindow::MySFMLWindow(int argc, char* argv[]) :
	window(sf::VideoMode(1280, 720), "SFML ", sf::Style::Titlebar | sf::Style::Close),  //blocking resize, not implemented yet
	left(false),
	right(false),
	conf(Config()), //When i start using it, I will move config file to appdata
	picFolder(findPicPath()),
	fileExplorator((argc > 1) ? (FolderExplorator(argv[1], conf.getExtenctions())) : (FolderExplorator(picFolder, conf.getExtenctions()))), //using picFolder var TO remember
	lastChange(std::clock())
{
	AsyncImageLoader::img.setSmooth(true); 
	sprite.setTexture(AsyncImageLoader::img); //I need to do this only once, sprite takes the pointer :O
}


bool MySFMLWindow::mainLoop() {
	if (fileExplorator.getPicCount() == 0) return EXIT_FAILURE;

	AsyncImageLoader::loadAsync(fileExplorator.getCurrentFilePath());
	window.setTitle(generateWindowName());

	while (window.isOpen())
	{
		EventLoop();
		ImageHandeling();
		KeyboardHandeling();
		if (exit) break;
	}
	return EXIT_SUCCESS;
}