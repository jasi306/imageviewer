#pragma once
#include <string>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <filesystem>
#include <cassert>
#include <sstream>
//TO DO
//add dynamic respons to changes in extenctions list
class FolderExplorator
{
	struct FileStruct
	{
		bool exit = false;
		int size;
		std::string path;
		std::filesystem::file_time_type date;
		std::string extension;
		std::string name;
		FileStruct(int size_, std::string path_, std::filesystem::file_time_type date_, std::string extension_, std::string name_) :size(size_), path(path_), date(date_), extension(extension_), name(name_) {}
		const std::string getStringDate() const;
		bool operator==(const FileStruct& other);
	}; 
	
	int pointer;
	std::vector<FileStruct > files;
	
public:
	friend std::ostream& operator<<(std::ostream& out, const FileStruct& f); //MyFile is private
	enum sortType {
		ByName,
		ByDate,
		BySize,
		ByType,
		ByNameReversed,
		ByDateReversed,
		BySizeReversed,
		ByTypeReversed
	};
	
	sortType currentSort;
	FolderExplorator() = delete;
	FolderExplorator(std::string path, std::vector<std::string> extenctions);

	void LoadFilesFromFolder(std::string path, std::vector<std::string> extenctions);
	bool sort(sortType t);
	std::string ChangeFile(int i);
	std::string getCurrentFilePath();
	std::string getCurrentFileName();
	std::string getCurrentFileLastChanges();
	void changeSort(int i);
	sortType GetCurrentSort();
	std::string currentSortToString();
	int getPicCount();
	//just for presentation
	int getCurrentNumberInVector();

#ifdef _DEBUG
	void showFiles() {
		for (auto file : files)
			std::cout << file;
	}
#endif
};

