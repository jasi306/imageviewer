#include "AsyncImageLoader.h"
typedef AsyncImageLoader AIL;


bool AIL::loading = false;

sf::Texture AIL::img = sf::Texture();
std::future<void> AIL::proces;

std::string AIL::loadedImgPath;
std::string AIL::targetImgPath;
bool AIL::isServed = false;

void AIL::load(std::string path) {
	img.loadFromFile(path);
	loadedImgPath = path;
	loading = false;
	isServed = false;
}
void AIL::loadAsync(std::string path) {
	targetImgPath = path;
#ifdef _DEBUG
	std::cout << "zmiana obrazka na nr " << path << std::endl;
#endif
	tryLoadAsync(path);
}
void AIL::tryLoadAsync(std::string path) {
	if (!loading) {
		loading = true;
		proces = std::async(std::launch::async, load, path);
	}
}

bool AIL::checkIfOk() {
	if (loadedImgPath != targetImgPath) {
		tryLoadAsync(targetImgPath);
		return 0;
	}
	return 1;
}

