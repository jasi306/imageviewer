#pragma once
#include "AsyncImageLoader.h"
#include "FolderExplorator.h"
#include "Config.h"

#include <iostream>
#include <SFML/Graphics.hpp>
#include <ctime>

#include <Windows.h>
#include <KnownFolders.h>
#include <shlobj.h>

class MySFMLWindow
{
	sf::RenderWindow window;
	bool left, right;
	Config conf;
	std::string picFolder;
	FolderExplorator fileExplorator;  //must be under picFolder
	sf::Sprite sprite;
	std::clock_t lastChange;
	bool exit;
	std::string findPicPath();
	std::string generateWindowName();
	
	void EventLoop();
	void ImageHandeling();
	void KeyboardHandeling();

public:
	MySFMLWindow(int argc, char* argv[]);
	bool mainLoop();

};

