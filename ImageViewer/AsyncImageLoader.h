#pragma once
#include "MyImage.h"
#include <future>
#include <string>
#include <iostream>
#include <SFML/Graphics.hpp>
class AsyncImageLoader
{
private:
	static std::future<void> proces;
	static void load(std::string path);
	static void tryLoadAsync(std::string path);

	static std::string loadedImgPath;
	static std::string targetImgPath;

	

public:
	static bool loading;
	static bool isServed;

	static sf::Texture img;
	static void loadAsync(std::string path);
	static bool checkIfOk(); 
};

