#include "Config.h"


bool Config::save() {
	std::ofstream configFileOut(path);

	json j;
	j[0] = legalExtenctions;
	j[1] = CooldownOnChangingPicInMs;
	configFileOut << j;
	

	configFileOut.close();
	return true;
}

void Config::setDefault() {
	legalExtenctions = std::vector<std::string>({
			".jpg",".png",".gif",".bmp",".jpeg" ,".jpe",".tif",".tiff",".dib"
		});
	CooldownOnChangingPicInMs = 500;
	save();
}


Config::Config(std::string  path_) : path(path_ + "\\.config") {
	std::ifstream configFileIn(path);
	if (configFileIn.good()) {
		json j;
		try {
			configFileIn >> j;
			legalExtenctions = j[0].get<std::vector<std::string>>();
			CooldownOnChangingPicInMs = j[1].get<int>();

		}
		catch (nlohmann::detail::parse_error e) {
			setDefault();
		}
	}
	else {
		setDefault();
	}
	configFileIn.close();
}


void Config::addExtenction(std::string ToAdd) {
	legalExtenctions.push_back(ToAdd);
	save();
}
void Config::eraseExtenction(std::string ToErase) {
	legalExtenctions.erase(std::find(legalExtenctions.begin(), legalExtenctions.end(), ToErase));
	save();
}
const std::vector<std::string> Config::getExtenctions() {
	return legalExtenctions;
}
int Config::getPicCahngeCooldown() {
	return CooldownOnChangingPicInMs;
}
void Config::setPicCahngeCooldown(int in) {
	CooldownOnChangingPicInMs = in;
	save();
}